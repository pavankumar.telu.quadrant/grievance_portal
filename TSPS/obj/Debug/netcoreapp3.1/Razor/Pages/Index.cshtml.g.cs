#pragma checksum "D:\Projects\SourceCode\TSPS\Pages\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7a8dbf3d0944661a09c03bc66ffadae44d214f5d"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(TSPS.Pages.Pages_Index), @"mvc.1.0.razor-page", @"/Pages/Index.cshtml")]
namespace TSPS.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Projects\SourceCode\TSPS\Pages\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Projects\SourceCode\TSPS\Pages\_ViewImports.cshtml"
using TSPS;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "D:\Projects\SourceCode\TSPS\Pages\_ViewImports.cshtml"
using TSPS.Data;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7a8dbf3d0944661a09c03bc66ffadae44d214f5d", @"/Pages/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"091a2bed4af777100d0186faef54654e4dbcb4ee", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Index : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 3 "D:\Projects\SourceCode\TSPS\Pages\Index.cshtml"
  
    ViewData["Title"] = "Grievance Dashboard";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<script type=""text/javascript"">
    //$(document).ready(function () {
    //    $('a').click(function (e) {
    //        alert('anchor clicked');
    //    });
    //}
    //    )
</script>
<div>
    <div class=""card-columns"">
        <div class=""card"">
            <div class=""card-title"">Total</div>
            <div class=""card-body"">100</div>
        </div>
        <div class=""card"">
            <div class=""card-title"">Active</div>
            <div class=""card-body"">44</div>
        </div>
        <div class=""card"">
            <div class=""card-title"">Escalated</div>
            <div class=""card-body"">6</div>
        </div>
        <div class=""card"">
            <div class=""card-title"">Closed</div>
            <div class=""card-body"">38</div>
        </div>
        <div class=""card"">
            <div class=""card-title"">Pending Verification</div>
            <div class=""card-body"">7</div>
        </div>
    </div>
    <div>
        <table class=""table"">
            <thead>
 ");
            WriteLiteral(@"               <tr>
                    <th>
                        Grievance ID
                    </th>
                    <th>Description</th>
                    <th>Submitted by</th>
                    <th>Submitted Date</th>
                    <th>Last updated on</th>
                    <th>Status</th>
                    <th>Comments</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><a href=""#"" data-toggle=""modal"" data-target=""#modalView""> 23123123</a></td>
                    <td>Roads not proper in Hyd</td>
                    <td>Ram</td>
                    <td>22/2/2020</td>
                    <td>8/8/2020</td>
                    <td>Pending with RDO</td>
                    <td>not enough details</td>
                    <td>
                        <a href=""#"" data-toggle=""modal"" data-target=""#modalTransfer"">Transfer</a>
                        <a href=""#"" data-toggle=""modal"" data-target=""#modalEscalate");
            WriteLiteral(@""">Escalate</a>
                        <a href=""#"" data-toggle=""modal"" data-target=""#modalUpdate"">Update</a>
                    </td>
                </tr>
                <tr>
                    <td><a href=""#"" data-toggle=""modal"" data-target=""#modalView"">443333</a></td>
                    <td>Electricity problem</td>
                    <td>Aron</td>
                    <td>22/2/2020</td>
                    <td>8/8/2020</td>
                    <td>Pending with RDO</td>
                    <td>not enough details</td>
                    <td>
                        <a href=""#"" data-toggle=""modal"" data-target=""#modalTransfer"">Transfer</a>
                        <a href=""#"" data-toggle=""modal"" data-target=""#modalEscalate"">Escalate</a>
                        <a href=""#"" data-toggle=""modal"" data-target=""#modalUpdate"">Update</a>
                    </td>
                </tr>
            </tbody>
        </table>

    </div>
</div>
<div id=""modalTransfer"" class=""modal fade"" role=""dia");
            WriteLiteral(@"log"">
    <div class=""modal-dialog"">
        <!-- Modal content-->
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h4 class=""modal-title"" style=""text-align-last: center"">Grievance Details</h4>
                <button type=""button"" class=""close"" data-dismiss=""modal"">&times;</button>
            </div>
            <div class=""modal-body"">
                <table>
                    <tr>
                        <td>
                            Grievance ID:
                        </td>
                        <td>
                            23123123
                        </td>
                    </tr>
                    <tr>
                        <td>Description: </td>
                        <td>Roads not proper in Hyd</td>
                    </tr>
                    <tr>
                        <td>Submitted by: </td>
                        <td>Ram</td>
                    </tr>
                    <tr>
                        <t");
            WriteLiteral(@"d>Submitted Date: </td>
                        <td>22/2/2020</td>
                    </tr>
                    <tr>
                        <td>Last updated on: </td>
                        <td>8/8/2020</td>
                        <td> </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Status: </td>
                        <td>Pending with RDO</td>
                        <td> </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Transfer to: </td>
                        <td>
                            <select id=""Select1"">
                                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7a8dbf3d0944661a09c03bc66ffadae44d214f5d8343", async() => {
                WriteLiteral(" --Select-- ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7a8dbf3d0944661a09c03bc66ffadae44d214f5d9339", async() => {
                WriteLiteral("Thasildar 1");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7a8dbf3d0944661a09c03bc66ffadae44d214f5d10334", async() => {
                WriteLiteral("Thasildar 2");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7a8dbf3d0944661a09c03bc66ffadae44d214f5d11330", async() => {
                WriteLiteral("Thasildar 3");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Comments: </td>
                        <td>
                            <textarea id=""TextArea1"" rows=""2"" cols=""20""></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <div class=""modal-footer"">
                <button type=""button"" class=""btn btn-default"" data-dismiss=""modal"">Update</button>
                <button type=""button"" class=""btn btn-default"" data-dismiss=""modal"">Clear</button>
            </div>
        </div>
    </div>
</div>
<div id=""modalEscalate"" class=""modal fade"" role=""dialog"">
    <div class=""modal-dialog"">
        <!-- Modal content-->
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h4 class=""modal-title"" style=""text-align-last: center"">Grievance Details</h4>
                <button type=""button"" class=""");
            WriteLiteral(@"close"" data-dismiss=""modal"">&times;</button>
            </div>
            <div class=""modal-body"">
                <table>
                    <tr>
                        <td>
                            Grievance ID:
                        </td>
                        <td>
                            23123123
                        </td>
                    </tr>
                    <tr>
                        <td>Description: </td>
                        <td>Roads not proper in Hyd</td>
                    </tr>
                    <tr>
                        <td>Submitted by: </td>
                        <td>Ram</td>
                    </tr>
                    <tr>
                        <td>Submitted Date: </td>
                        <td>22/2/2020</td>
                    </tr>
                    <tr>
                        <td>Last updated on: </td>
                        <td>8/8/2020</td>
                        <td> </td>
                        <td></td>
");
            WriteLiteral(@"                    </tr>
                    <tr>
                        <td>Status: </td>
                        <td>Pending with RDO</td>
                        <td> </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Escalate to: </td>
                        <td>
                            <select id=""Select1"">
                                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7a8dbf3d0944661a09c03bc66ffadae44d214f5d14881", async() => {
                WriteLiteral(" --Select-- ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7a8dbf3d0944661a09c03bc66ffadae44d214f5d15878", async() => {
                WriteLiteral("RDO");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7a8dbf3d0944661a09c03bc66ffadae44d214f5d16866", async() => {
                WriteLiteral("District Collector");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Comments: </td>
                        <td>
                            <textarea id=""TextArea1"" rows=""2"" cols=""20""></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <div class=""modal-footer"">
                <button type=""button"" class=""btn btn-default"" data-dismiss=""modal"">Update</button>
                <button type=""button"" class=""btn btn-default"" data-dismiss=""modal"">Clear</button>
            </div>
        </div>
    </div>
</div>
<div id=""modalUpdate"" class=""modal fade"" role=""dialog"">
    <div class=""modal-dialog"">
        <!-- Modal content-->
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h4 class=""modal-title"" style=""text-align-last: center"">Update Grievance</h4>
                <button type=""button"" class=""clo");
            WriteLiteral(@"se"" data-dismiss=""modal"">&times;</button>
            </div>
            <div class=""modal-body"">
                <table>
                    <tr>
                        <td>
                            Grievance ID:
                        </td>
                        <td>
                            23123123
                        </td>
                    </tr>
                    <tr>
                        <td>Description: </td>
                        <td>Roads not proper in Hyd</td>
                    </tr>
                    <tr>
                        <td>Submitted by: </td>
                        <td>Ram</td>
                    </tr>
                    <tr>
                        <td>Submitted Date: </td>
                        <td>22/2/2020</td>
                    </tr>
                    <tr>
                        <td>Last updated on: </td>
                        <td>8/8/2020</td>
                        <td> </td>
                        <td></td>
   ");
            WriteLiteral("                 </tr>\r\n                    <tr>\r\n                        <td>Status: </td>\r\n                        <td>\r\n                            <select id=\"Select1\">\r\n                                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7a8dbf3d0944661a09c03bc66ffadae44d214f5d20207", async() => {
                WriteLiteral(" --Select-- ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7a8dbf3d0944661a09c03bc66ffadae44d214f5d21204", async() => {
                WriteLiteral("Close");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7a8dbf3d0944661a09c03bc66ffadae44d214f5d22194", async() => {
                WriteLiteral("Reject");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("option", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "7a8dbf3d0944661a09c03bc66ffadae44d214f5d23185", async() => {
                WriteLiteral("In Progress");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.OptionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_OptionTagHelper);
            BeginWriteTagHelperAttribute();
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __tagHelperExecutionContext.AddHtmlAttribute("selected", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.Minimized);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Comments: </td>
                        <td>
                            <textarea id=""TextArea1"" rows=""2"" cols=""20""></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <div class=""modal-footer"">
                <button type=""button"" class=""btn btn-default"" data-dismiss=""modal"">Update</button>
                <button type=""button"" class=""btn btn-default"" data-dismiss=""modal"">Clear</button>
            </div>
        </div>
    </div>
</div>
<div id=""modalView"" class=""modal fade"" role=""dialog"">
    <div class=""modal-dialog"">
        <!-- Modal content-->
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h4 class=""modal-title"" style=""text-align-last: center"">Grievance Details</h4>
                <button type=""button"" class=""clos");
            WriteLiteral(@"e"" data-dismiss=""modal"">&times;</button>
            </div>
            <div class=""modal-body"">
                <table>
                    <tr>
                        <td>
                            Grievance ID:
                        </td>
                        <td>
                            23123123
                        </td>
                    </tr>
                    <tr>
                        <td>Description: </td>
                        <td>Roads not proper in Hyd</td>
                    </tr>
                    <tr>
                        <td>Submitted by: </td>
                        <td>Ram</td>
                    </tr>
                    <tr>
                        <td>Submitted Date: </td>
                        <td>22/2/2020</td>
                    </tr>
                    <tr>
                        <td>Last updated on: </td>
                        <td>8/8/2020</td>
                        <td> </td>
                        <td></td>
    ");
            WriteLiteral(@"                </tr>
                    <tr>
                        <td>Status: </td>
                        <td>Pending with RDO</td>
                        <td> </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Comments: </td>
                        <td>not enough details</td>
                    </tr>
                </table>
            </div>
            <div class=""modal-footer"">
                <button type=""button"" class=""btn btn-default"" data-dismiss=""modal"">Close</button>
            </div>
        </div>
    </div>
</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IndexModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel>)PageContext?.ViewData;
        public IndexModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
