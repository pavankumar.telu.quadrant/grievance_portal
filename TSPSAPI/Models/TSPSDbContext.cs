﻿using Microsoft.EntityFrameworkCore;

namespace TSPSAPI.Models
{
    public class TSPSDbContext:DbContext
    {
        public TSPSDbContext(DbContextOptions<TSPSDbContext> options) : base(options) { }

        public DbSet<Grievance> Grievances { get; set; }
        public DbSet<Test> Tests { get; set; }
    }
}