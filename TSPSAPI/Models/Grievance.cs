﻿namespace TSPSAPI.Models
{
    public class Test
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class Grievance
    {
        public int ID { get; set; }
        public string LoginID { get; set; }
        public int GrievanceID { get; set; }
        public string PersonName { get; set; }
        public int RoleID { get; set; }
        public int DepartmentID { get; set; }
        public int TalukaID { get; set; }
        public string Village { get; set; }
        public string Description { get; set; }
        public string EventDate { get; set; }
        public string FileRelativeUrl { get; set; }
        public int CurrentStatusID { get; set; }
    }

}