﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TSPSAPI.Models;

namespace TSPSAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GrievancesController : ControllerBase
    {
        private readonly TSPSDbContext _context;

        public GrievancesController(TSPSDbContext context)
        {
            _context = context;
        }

        // GET: api/Grievances
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Grievance>>> GetGrievances()
        {
            return await _context.Grievances.ToListAsync();
        }

        // GET: api/Grievances/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Grievance>> GetGrievance(int id)
        {
            var grievance = await _context.Grievances.FindAsync(id);

            if (grievance == null)
            {
                return NotFound();
            }

            return grievance;
        }

        // PUT: api/Grievances/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGrievance(int id, Grievance grievance)
        {
            if (id != grievance.ID)
            {
                return BadRequest();
            }

            _context.Entry(grievance).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GrievanceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Grievances
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Grievance>> PostGrievance(Grievance grievance)
        {
            _context.Grievances.Add(grievance);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGrievance", new { id = grievance.ID }, grievance);
        }

        // DELETE: api/Grievances/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Grievance>> DeleteGrievance(int id)
        {
            var grievance = await _context.Grievances.FindAsync(id);
            if (grievance == null)
            {
                return NotFound();
            }

            _context.Grievances.Remove(grievance);
            await _context.SaveChangesAsync();

            return grievance;
        }

        private bool GrievanceExists(int id)
        {
            return _context.Grievances.Any(e => e.ID == id);
        }
    }
}
