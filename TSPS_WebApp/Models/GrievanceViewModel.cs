﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TSPS_WebApp.Models
{
    public class GrievanceViewModel
    {
        public int LoginId { get; set; }
        public string GrievanceId { get; set; }
        [DisplayName("Name")]
        public string Name { get; set; }
        public int RoleId { get; set; }
        [DisplayName("Department")]
        public string Dept { get; set; }
        public string Taluka { get; set; }
        public string Village { get; set; }
        public string Description { get; set; }
        public string FileRelativeUrl { get; set; }
        public string CurrentStatus { get; set; }
        public DateTime LastModifiedAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CreatedBy { get; set; }
        public string Status { get; set; }
        public List<Dept> Depts { get; set; }
        public List<Taluka> Talukas { get; set; }
        public List<Village> Villages { get; set; }
        public List<GrievanceViewModel> grievanceViewModels{get; set;}
    }

    public class Dept
    {
        [DisplayName("Id")]
        public int Id { get; set; }
        [DisplayName("Name")]
        public string Name { get; set; }
    }
    public class Taluka
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class Village
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
