﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TSPS_WebApp.Models;

namespace TSPS_WebApp.Controllers
{
    public class FileGrievanceController : Controller
    {
        // GET: FileComplaintController
        public ActionResult Index()
        {
            GrievanceViewModel grievanceViewModel = new GrievanceViewModel();
            SelectListItem deptSelectListItem = new SelectListItem("Municipality", "1");
            deptSelectListItem = new SelectListItem("Revenue", "2");
            Dept dept = new Dept();
            grievanceViewModel.Depts = new List<Dept>();
            grievanceViewModel.Depts.Add(new Dept() { Name = "Municipality", Id = 1 });
            grievanceViewModel.Depts.Add(new Dept() { Name = "Revenue", Id = 2 });

            Taluka taluka = new Taluka();
            grievanceViewModel.Talukas = new List<Taluka>(){
                new Taluka() { Id = 1, Name = "Lingampalli" },
                 new Taluka() { Id = 2, Name = "Gachibowli" } };

            Village village= new Village();
            grievanceViewModel.Villages = new List<Village>(){
                new Village() { Id = 1, Name = "Sriram Nagar" },
                 new Village() { Id = 2, Name = "Kondapur" } };

            return View(grievanceViewModel);
        }

        // GET: FileComplaintController/Details/5
        public ActionResult TrackComplaint(int id)
        {
            return View();
        }

        // GET: FileComplaintController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FileComplaintController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: FileComplaintController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: FileComplaintController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: FileComplaintController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: FileComplaintController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
